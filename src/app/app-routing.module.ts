import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('./pages/search-employee-page/search-employee-page.module').then(m => m.SearchEmployeePageModule)
  },
  {
    path: 'add',
    loadChildren: () => import('./pages/add-employee-page/add-employee-page.module').then(m => m.AddEmployeePageModule)
  },
  // {
  //   path: 'search',
  //   loadChildren: () => import('./pages/search-employee-page/search-employee-page.module').then(m => m.SearchEmployeePageModule)
  // },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
