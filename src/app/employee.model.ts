export class Employee {
    constructor( 
        public code: number,
        public name: string,
        public department: string,
        public birthDate: string,
        public gender: string
        ) {
    }
}