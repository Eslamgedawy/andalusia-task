import { Injectable } from '@angular/core';
import { Employee } from './employee.model';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {

  employees: Employee[] = [
    new Employee(
      1,
      'ali',
      'Sales',
      new Date('December 22, 1995').toDateString(),
      'Male'
    ),
    new Employee(
      2,
      'eslam',
      'IT',
      new Date('March 17, 1996').toDateString(),
      'Male'
    ),
    new Employee(
      3,
      'aya',
      'Sales',
      new Date('March 17, 1955').toDateString(),
      'Female'
    ),
    // new Employee(
    //   4,
    //   'bassant',
    //   'HR',
    //   new Date('March 17, 1989').toDateString(),
    //   'Female'
    // ),
    // new Employee(
    //   5,
    //   'hany',
    //   'IT',
    //   new Date('Feb 17, 1999').toDateString(),
    //   'Male'
    // ),
  ]
  constructor() { }

      // get all Employees
      getEmployees(){
        return [...this.employees];
      }

      // add employee
      addEmp(employee: Employee){
        this.employees.push(employee);
      }
      // search in employees
      searchEmp(name: string, dept: string){
          this.employees.filter((name , dept) =>{

          })
      }
}
