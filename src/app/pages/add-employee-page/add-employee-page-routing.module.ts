import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddEmployeePageComponent } from './add-employee-page.component';


const routes: Routes = [
  {
    path: '',
    component: AddEmployeePageComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AddEmployeePageRoutingModule { }
