import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Employee } from '../../employee.model';
import { EmployeeService } from '../../employee.service';

@Component({
  selector: 'app-add-employee-page',
  templateUrl: './add-employee-page.component.html',
  styleUrls: ['./add-employee-page.component.scss']
})
export class AddEmployeePageComponent implements OnInit {
  myForm: FormGroup;
  // formB: FormGroup;
  employees: Employee[] = [];
  genders = ['male', 'female'];

  msg: any;
  submitted= false;
  alert: any;

  constructor(private empService: EmployeeService,
              private fb: FormBuilder) {
  }

  ngOnInit() {
    this.formInit();
  }

  formInit(){
    let code = '';
    let name = '';
    let dept = '';
    let gender = '';
    let date = '';
    this.myForm = new FormGroup({
      'code': new FormControl(code,Validators.required),
      'name': new FormControl(name,Validators.required),
      'department': new FormControl(dept,Validators.required),
      'birthDate': new FormControl(date,Validators.required),
      // 'gender': new FormControl('male'),
      'gender': new FormBuilder().control(null,Validators.required)
    })
    // this.formB = this.fb.group({
    //   'gender': [gender, Validators.required]
    // });
  }

  onSubmit(){ 
    console.log(this.myForm);
      this.empService.addEmp(this.myForm.value);
      this.employees = this.empService.getEmployees();
      console.log(this.employees);
      if (this.myForm.valid) {
          this.submitted= true; 
          this.myForm.reset();
      }
  }


  
  
  closeAlert(){
    this.submitted = false;
  }

}
