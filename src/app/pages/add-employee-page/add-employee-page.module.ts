import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AddEmployeePageRoutingModule } from './add-employee-page-routing.module';
import { AddEmployeePageComponent } from './add-employee-page.component';
import { SharedComponentsModule } from '../../shared/components/shared-components.module';
import { ReactiveFormsModule } from '@angular/forms';

// translation
import { HttpClient } from '@angular/common/http';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, 'assets/i18n/', '.json')
}
@NgModule({
  declarations: [AddEmployeePageComponent],
  imports: [
    CommonModule,
    AddEmployeePageRoutingModule,
    SharedComponentsModule,
    ReactiveFormsModule,
    TranslateModule
  ]
})
export class AddEmployeePageModule { }
