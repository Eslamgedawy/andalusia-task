import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SearchEmployeePageComponent } from './search-employee-page.component';


const routes: Routes = [
  {
    path: '',
    component: SearchEmployeePageComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SearchEmployeePageRoutingModule { }
