import { Component, OnInit } from '@angular/core';
import { Employee } from 'src/app/employee.model';
import { EmployeeService } from '../../employee.service';

@Component({
  selector: 'app-search-employee-page',
  templateUrl: './search-employee-page.component.html',
  styleUrls: ['./search-employee-page.component.scss']
})
export class SearchEmployeePageComponent implements OnInit {

  searchText;
  employees: Employee[] = [];

  constructor(private empServices: EmployeeService,) {
  }

  ngOnInit() {
    this.employees = this.empServices.getEmployees();
  }
}
