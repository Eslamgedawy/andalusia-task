import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SearchEmployeePageRoutingModule } from './search-employee-page-routing.module';
import { SearchEmployeePageComponent } from './search-employee-page.component';
import { SharedComponentsModule } from '../../shared/components/shared-components.module';

// translation
import { HttpClient } from '@angular/common/http';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, 'assets/i18n/', '.json')
}

@NgModule({
  declarations: [SearchEmployeePageComponent],
  imports: [
    CommonModule,
    SearchEmployeePageRoutingModule,
    SharedComponentsModule,
    TranslateModule
  ]
})
export class SearchEmployeePageModule { }
