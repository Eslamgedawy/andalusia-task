import { DOCUMENT } from '@angular/common';
import { Component, Inject, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  cuurentLang: string;

  constructor(
    public translate: TranslateService,
    @Inject(DOCUMENT) private document: Document
    ) {
    translate.addLangs(["en", "ar"]);
    translate.setDefaultLang('en');
    // let browserLang = translate.getBrowserLang();
    // translate.use(browserLang.match(/en|ar/) ? browserLang : 'en');

    this.cuurentLang = localStorage.getItem('lang') || 'en';
    this.useLanguage(this.cuurentLang);
    translate.use(this.cuurentLang);
  }

  ngOnInit() {
  }

useLanguage(language: string) {
    let htmlTag = this.document.getElementsByTagName("html")[0] as HTMLHtmlElement;
    htmlTag.dir = language === "ar" ? "rtl" : "ltr";
    this.translate.setDefaultLang(language);
    this.translate.use(language);
    localStorage.setItem('lang', language);
}

relaod(language: string){
  this.useLanguage(language);
  // window.location.reload();
}

}
